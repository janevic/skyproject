package com.cyborg.skytestproject;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class TrackISSActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap theMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.the_map_fragment);

        if(isNetworkAvailable()) {
            /*theMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.the_map)).getMap();
            if(theMap != null) {
                theMap.getUiSettings().setZoomControlsEnabled(true);

                callTrackISSAsyncTask();
            }*/
            MapFragment mapFragment  = (MapFragment) getFragmentManager().findFragmentById(R.id.the_map);
            mapFragment.getMapAsync(TrackISSActivity.this);
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    TrackISSActivity.this);
            alertDialogBuilder.setTitle("Internet connection");
            alertDialogBuilder
                    .setMessage("This app needs internet connection.")
                    .setCancelable(false);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if(item.getItemId() == R.id.action_NEO) {
            //Near Earth Objects
            Intent neoIntent = new Intent(this, NEOActivity.class);
            startActivity(neoIntent);
        }
        else if(item.getItemId() == R.id.action_image) {
            //Image of the day

            Intent imageIntent = new Intent(this, ImageActivity.class);
            startActivity(imageIntent);

        } else if(item.getItemId() == R.id.action_iss) {
            //Track ISS

        } else {
            // Settings
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);

        }

        return super.onOptionsItemSelected(item);
    }

    public void callTrackISSAsyncTask() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsyncTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        TrackISSAsyncTask trackISSAsyncTask = new TrackISSAsyncTask();
                        trackISSAsyncTask.execute();
                    }
                });
            }
        };
        timer.schedule(doAsyncTask, 0, 5000);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        theMap = googleMap;
        theMap.getUiSettings().setZoomControlsEnabled(true);
        callTrackISSAsyncTask();

    }

    private class TrackISSAsyncTask extends AsyncTask<String, String, String> {

        String jsonStringUrl;
        private TrackISS trackISS;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            jsonStringUrl = "http://api.open-notify.org/iss-now.json";
        }

        @Override
        protected String doInBackground(String... params) {

            JsonParser jsonParser = new JsonParser();
            JSONObject jsonObject = jsonParser.getJSONFromUrl(jsonStringUrl);

            try {
                JSONObject iss_position = jsonObject.getJSONObject("iss_position");

                String lat = iss_position.getString("latitude");
                String lng = iss_position.getString("longitude");
                String msg = jsonObject.getString("message");

                trackISS = new TrackISS(lat, lng, msg);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(trackISS.getSuccess().equals("success")) {
                double lat = Double.parseDouble(trackISS.getLatitude());
                double lng = Double.parseDouble(trackISS.getLongitude());

                LatLng latLng = new LatLng(lat, lng);

                theMap.clear();
                theMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                theMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.iss3)));
            }
        }
    }
}
