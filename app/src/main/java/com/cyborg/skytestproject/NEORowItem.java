package com.cyborg.skytestproject;

/**
 * Created by Bobi on 2/9/2016.
 */
public class NEORowItem {

    private String name;
    private String date;
    private String size;
    private String hazard;
    private String lunar;

    public NEORowItem(){}

    public NEORowItem(String name, String date, String size, String hazard, String lunar) {
        this.name = name;
        this.date = date;
        this.size = size;
        this.hazard = hazard;
        this.lunar = lunar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getLunar() {
        return lunar;
    }

    public void setLunar(String lunar) {
        this.lunar = lunar;
    }

    public String getHazard() {
        return hazard;
    }

    public void setHazard(String hazard) {
        this.hazard = hazard;
    }

}
