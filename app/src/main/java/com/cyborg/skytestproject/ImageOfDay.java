package com.cyborg.skytestproject;

import android.graphics.Bitmap;

/**
 * Created by Darko on 16-Feb-16.
 */
public class ImageOfDay {
    private String date;
    private String name;
    private String explanation;
    private String url;
    private Bitmap bitmap;
    private String media_type;

    public ImageOfDay() {
    }

    public ImageOfDay(String date, String name, String explanation, String url, Bitmap bitmap, String media_type) {
        this.date = date;
        this.name = name;
        this.explanation = explanation;
        this.url = url;
        this.bitmap = bitmap;
        this.media_type = media_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

}
