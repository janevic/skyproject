package com.cyborg.skytestproject;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Bobi on 2/19/2016.
 */
public class BroadcastReceiver extends android.content.BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, ImageWallpaperService.class);
        context.startService(i);
    }
}
