package com.cyborg.skytestproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class NEOActivityDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neoactivity_details);

        Bundle bundle = getIntent().getExtras();

        TextView textViewDate = (TextView) findViewById(R.id.date);
        String date = bundle.getString("date");
        System.out.println(date);
        textViewDate.setText(date);

        TextView textViewName = (TextView) findViewById(R.id.name);
        String name = bundle.getString("name");
        System.out.println(name);
        textViewName.setText(name);

        TextView textViewDiameterMin = (TextView) findViewById(R.id.estimated_diameter_meters_min);
        String diameter_min = bundle.getString("diameter_min");
        textViewDiameterMin.setText(diameter_min);

        TextView textViewDiameterMax = (TextView) findViewById(R.id.estimated_diameter_meters_max);
        String diameter_max = bundle.getString("diameter_max");
        textViewDiameterMax.setText(diameter_max);

        TextView textViewHazardous = (TextView) findViewById(R.id.potentially_hazardous);
        String hazardous = bundle.getString("hazardous");
        textViewHazardous.setText(hazardous);

        TextView textViewApproachDate = (TextView) findViewById(R.id.close_approach_date);
        String approach_date = bundle.getString("approach_date");
        textViewApproachDate.setText(approach_date);

        TextView textViewKph = (TextView) findViewById(R.id.kilometers_per_hour);
        String kph = bundle.getString("kilometers_per_hour");
        textViewKph.setText(kph);

        TextView textViewLunar = (TextView) findViewById(R.id.miss_distance_lunar);
        String lunar = bundle.getString("distance_lunar");
        textViewLunar.setText(lunar);

        TextView textViewOrbitingBody = (TextView) findViewById(R.id.orbiting_body);
        String body = bundle.getString("orbiting_body");
        textViewOrbitingBody.setText(body);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_NEO) {
            //Near Earth Objects
            Intent neoIntent = new Intent(this, NEOActivity.class);
            startActivity(neoIntent);
        }
        else if(item.getItemId() == R.id.action_image) {
            //Image of the day
            Intent imageIntent = new Intent(this, ImageActivity.class);
            startActivity(imageIntent);

        } else if(item.getItemId() == R.id.action_iss) {
            //Track ISS
            Intent issIntent = new Intent(this, TrackISSActivity.class);
            startActivity(issIntent);

        } else {
            // Settings
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }


        return false;
    }

}
