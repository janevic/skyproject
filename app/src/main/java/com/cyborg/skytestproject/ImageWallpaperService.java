package com.cyborg.skytestproject;

import android.app.IntentService;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Bobi on 2/18/2016.
 */
public class ImageWallpaperService extends IntentService {



    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * name Used to name the worker thread, important only for debugging.
     */
    public ImageWallpaperService() {
        super("ImageWallpaperService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // This describes what will happen when service is triggered
        if(isNetworkAvailable()) {
            String jsonStringUrl = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";

            JsonParser jsonParser = new JsonParser();
            JSONObject jsonObject = jsonParser.getJSONFromUrl(jsonStringUrl);

            try {
                String url = jsonObject.getString("url");
                String type = jsonObject.getString("media_type");

                if(type.equals("image")) {
                    WallpaperManager wpm = WallpaperManager.getInstance(getApplicationContext());
                    InputStream inputStream = new URL(url).openStream();
                    wpm.setStream(inputStream);
                    inputStream.close();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        else {
            return false;
        }
    }


}
