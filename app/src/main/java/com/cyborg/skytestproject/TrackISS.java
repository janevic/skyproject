package com.cyborg.skytestproject;

/**
 * Created by Bobi on 2/17/2016.
 */
public class TrackISS {
    private String latitude;
    private String longitude;
    private String success;

    public TrackISS() {
    }

    public TrackISS(String latitude, String longitude, String success) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.success = success;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
