package com.cyborg.skytestproject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NEOActivity extends AppCompatActivity {

    private ListView neoListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neo);

        if(isNetworkAvailable()) {
            new NEOAsyncTask().execute();
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    NEOActivity.this);
            alertDialogBuilder.setTitle("Internet connection");
            alertDialogBuilder
                    .setMessage("This app needs internet connection.")
                    .setCancelable(false);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_NEO) {
            //Near Earth Objects

        }
        else if(item.getItemId() == R.id.action_image) {
            //Image of the day
            Intent imageIntent = new Intent(this, ImageActivity.class);
            startActivity(imageIntent);

        } else if(item.getItemId() == R.id.action_iss) {
            //Track ISS
            Intent issIntent = new Intent(this, TrackISSActivity.class);
            startActivity(issIntent);

        } else {
            // Settings
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }


        return false;
    }

    private class NEOAsyncTask extends AsyncTask<String, String, String> {

        String jsonStringUrl;
        List<NearEarthObject> neoList;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(NEOActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            Calendar calendar = Calendar.getInstance();
            Date today = calendar.getTime();

            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = calendar.getTime();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String startDate = dateFormat.format(today);
            String endDate = dateFormat.format(tomorrow);

            jsonStringUrl = "https://api.nasa.gov/neo/rest/v1/feed?start_date="+startDate+"&end_date="+endDate+"&api_key=DEMO_KEY";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {
            neoList = new ArrayList<>();
            JsonParser jsonParser = new JsonParser();
            JSONObject jsonObject = jsonParser.getJSONFromUrl(jsonStringUrl);

            Calendar calendar = Calendar.getInstance();
            Date today = calendar.getTime();

            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = calendar.getTime();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String startDate = dateFormat.format(today);
            String endDate = dateFormat.format(tomorrow);

            getItemForDate(jsonObject, startDate);
            getItemForDate(jsonObject, endDate);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            neoListView = (ListView) findViewById(R.id.neoList);
            neoListView.setItemsCanFocus(true);
            neoListView.setAdapter(new MyBaseAdapter(getApplicationContext(), neoList));

            neoListView.setClickable(true);
            neoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(view.getContext(), NEOActivityDetails.class);
                    NearEarthObject nearEarthObject = neoList.get(position);
                    intent.putExtra("date", nearEarthObject.getDate());
                    intent.putExtra("name", nearEarthObject.getName());
                    intent.putExtra("diameter_min", nearEarthObject.getEstimated_diameter_meters_min());
                    intent.putExtra("diameter_max", nearEarthObject.getEstimated_diameter_meters_max());
                    intent.putExtra("hazardous", nearEarthObject.getPotentially_hazardous());
                    intent.putExtra("approach_date", nearEarthObject.getClose_approach_date());
                    intent.putExtra("kilometers_per_hour", nearEarthObject.getKilometers_per_hour());
                    intent.putExtra("distance_lunar", nearEarthObject.getMiss_distance_lunar());
                    intent.putExtra("orbiting_body", nearEarthObject.getOrbiting_body());
                    startActivity(intent);
                }
            });

            progressDialog.dismiss();
        }

        private void getItemForDate(JSONObject jsonObject ,String date) {

            NearEarthObject nearEarthObject;
            try {
                JSONObject jsonObject1 = jsonObject.getJSONObject("near_earth_objects");
                JSONArray firstDate = jsonObject1.getJSONArray(date);
                for(int i=0; i<firstDate.length(); i++) {
                    nearEarthObject = new NearEarthObject();
                    nearEarthObject.setDate(date);//setDate
                    JSONObject firstJsonItem = firstDate.getJSONObject(i);
                    nearEarthObject.setName(firstJsonItem.getString("name")); //setName
                    JSONObject estimatedDiameter = firstJsonItem.getJSONObject("estimated_diameter");
                    JSONObject meters = estimatedDiameter.getJSONObject("meters");

                    String minSize = meters.getString("estimated_diameter_min");
                    double minSizeMeters = Double.parseDouble(minSize);
                    nearEarthObject.setEstimated_diameter_meters_min("" + String.format("%.2f", minSizeMeters) + " meters"); // estimated diameter min

                    String maxSize = meters.getString("estimated_diameter_max");
                    double maxSizeMeters = Double.parseDouble(maxSize);
                    nearEarthObject.setEstimated_diameter_meters_max(""+String.format("%.2f", maxSizeMeters) + " meters"); // estimated diameter max

                    nearEarthObject.setPotentially_hazardous(firstJsonItem.getString("is_potentially_hazardous_asteroid"));

                    JSONArray closeApproachData = firstJsonItem.getJSONArray("close_approach_data");
                    JSONObject closeApproachDataItem0 = closeApproachData.getJSONObject(0);

                    nearEarthObject.setClose_approach_date(closeApproachDataItem0.getString("close_approach_date"));

                    JSONObject relativeVelocity = closeApproachDataItem0.getJSONObject("relative_velocity");

                    String speed = relativeVelocity.getString("kilometers_per_hour");
                    double d = Double.parseDouble(speed);
                    nearEarthObject.setKilometers_per_hour(""+String.format("%.2f", d));

                    JSONObject missDistance = closeApproachDataItem0.getJSONObject("miss_distance");

                    String miss = missDistance.getString("lunar");
                    double missLunar = Double.parseDouble(miss);
                    nearEarthObject.setMiss_distance_lunar(""+String.format("%.5f", missLunar));

                    nearEarthObject.setOrbiting_body(closeApproachDataItem0.getString("orbiting_body"));

                    neoList.add(nearEarthObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
