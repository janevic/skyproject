package com.cyborg.skytestproject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final SharedPreferences sharedPreferences = this.getSharedPreferences("com.cyborg.skytestproject", Context.MODE_PRIVATE);

        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.setImageWallpaper);

        final String wallpaperOnOff = "com.cyborg.skytestproject.wallpaperOnOff";
        String data = sharedPreferences.getString(wallpaperOnOff, "OFF");

        if(data.equals("OFF")) {
            toggleButton.setChecked(false);
        }
        else {
            toggleButton.setChecked(true);
        }

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(wallpaperOnOff, "ON");
                    editor.commit();

                    //Set Alarm ON
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    Intent intent = new Intent(SettingsActivity.this, BroadcastReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(SettingsActivity.this, BroadcastReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(System.currentTimeMillis());
                    cal.set(Calendar.HOUR_OF_DAY, 10);
                    cal.set(Calendar.MINUTE, 10);
                    cal.set(Calendar.SECOND, 0);
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        //Log.d("USING", ">= KITKAT");
                        Toast.makeText(
                                getApplicationContext(),
                                "ON - New wallpaper will be set every day around 10 AM",
                                Toast.LENGTH_SHORT).show();

                        alarmManager.setExact(
                                AlarmManager.RTC_WAKEUP,
                                cal.getTimeInMillis(),
                                pendingIntent);
                    }
                    else {
                        //Log.d("USING", "< KITKAT");
                        Toast.makeText(
                                getApplicationContext(),
                                "ON - New wallpaper will be set every day",
                                Toast.LENGTH_SHORT).show();

                        alarmManager.setRepeating(
                                AlarmManager.RTC_WAKEUP,
                                cal.getTimeInMillis(),
                                AlarmManager.INTERVAL_DAY,
                                pendingIntent);
                    }*/

                    alarmManager.setInexactRepeating(
                            AlarmManager.RTC_WAKEUP,
                            cal.getTimeInMillis(),
                            AlarmManager.INTERVAL_DAY,
                            pendingIntent);

                    Toast.makeText(
                            getApplicationContext(),
                            "ON - New wallpaper will be set every day",
                            Toast.LENGTH_SHORT).show();


                }
                else {
                    Toast.makeText(getApplicationContext(), "OFF - No wallpaper will be set", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(wallpaperOnOff, "OFF");
                    editor.commit();

                    //Set Alarm OFF
                    Intent intent = new Intent(getApplicationContext(), BroadcastReceiver.class);
                    final PendingIntent pendingIntent = PendingIntent.getBroadcast(SettingsActivity.this, BroadcastReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) SettingsActivity.this.getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(pendingIntent);

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_NEO) {
            //Near Earth Objects
            Intent neoIntent = new Intent(this, NEOActivity.class);
            startActivity(neoIntent);
        }
        else if(item.getItemId() == R.id.action_image) {
            //Image of the day
            Intent imageIntent = new Intent(this, ImageActivity.class);
            startActivity(imageIntent);

        } else if(item.getItemId() == R.id.action_iss) {
            //Track ISS
            Intent issIntent = new Intent(this, TrackISSActivity.class);
            startActivity(issIntent);

        } else {
            // Settings

        }

        return false;
    }
}
