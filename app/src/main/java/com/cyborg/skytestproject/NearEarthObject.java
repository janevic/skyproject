package com.cyborg.skytestproject;

/**
 * Created by Bobi on 2/14/2016.
 */
public class NearEarthObject {

    private String date;
    private String name;
    private String estimated_diameter_meters_min;
    private String estimated_diameter_meters_max;
    private String potentially_hazardous;
    private String close_approach_date;
    private String kilometers_per_hour;
    private String miss_distance_lunar;
    private String orbiting_body;

    public NearEarthObject() {
    }

    public NearEarthObject(String date, String name, String estimated_diameter_meters_min, String estimated_diameter_meters_max, String potentially_hazardous, String close_approach_date, String kilometers_per_hour, String orbiting_body, String miss_distance_lunar) {
        this.date = date;
        this.name = name;
        this.estimated_diameter_meters_min = estimated_diameter_meters_min;
        this.estimated_diameter_meters_max = estimated_diameter_meters_max;
        this.potentially_hazardous = potentially_hazardous;
        this.close_approach_date = close_approach_date;
        this.kilometers_per_hour = kilometers_per_hour;
        this.orbiting_body = orbiting_body;
        this.miss_distance_lunar = miss_distance_lunar;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEstimated_diameter_meters_min() {
        return estimated_diameter_meters_min;
    }

    public void setEstimated_diameter_meters_min(String estimated_diameter_meters_min) {
        this.estimated_diameter_meters_min = estimated_diameter_meters_min;
    }

    public String getEstimated_diameter_meters_max() {
        return estimated_diameter_meters_max;
    }

    public void setEstimated_diameter_meters_max(String estimated_diameter_meters_max) {
        this.estimated_diameter_meters_max = estimated_diameter_meters_max;
    }

    public String getPotentially_hazardous() {
        return potentially_hazardous;
    }

    public void setPotentially_hazardous(String potentially_hazardous) {
        this.potentially_hazardous = potentially_hazardous;
    }

    public String getClose_approach_date() {
        return close_approach_date;
    }

    public void setClose_approach_date(String close_approach_date) {
        this.close_approach_date = close_approach_date;
    }

    public String getKilometers_per_hour() {
        return kilometers_per_hour;
    }

    public void setKilometers_per_hour(String kilometers_per_hour) {
        this.kilometers_per_hour = kilometers_per_hour;
    }

    public String getMiss_distance_lunar() {
        return miss_distance_lunar;
    }

    public void setMiss_distance_lunar(String miss_distance_lunar) {
        this.miss_distance_lunar = miss_distance_lunar;
    }

    public String getOrbiting_body() {
        return orbiting_body;
    }

    public void setOrbiting_body(String orbiting_body) {
        this.orbiting_body = orbiting_body;
    }
}
