package com.cyborg.skytestproject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Bobi on 2/9/2016.
 */
public class MyBaseAdapter extends BaseAdapter {

    private Context context;
    private List<NearEarthObject> neoRowListItems;

    public MyBaseAdapter(Context context, List<NearEarthObject> neoRowListItems) {
        this.context = context;
        this.neoRowListItems = neoRowListItems;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView textViewName;

        TextView textViewDate;
        TextView textViewDate2;

        TextView textViewSize;
        TextView textViewSize2;

        //TextView textViewHazardous;
        //TextView textViewHazardous2;

        TextView textViewLunar;
        TextView textViewLunar2;
    }

    @Override
    public int getCount() {
        return neoRowListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return neoRowListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return neoRowListItems.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.neo_list_item_layout, null);
            holder = new ViewHolder();

            //holder.imageView = (ImageView) convertView.findViewById(R.id.neoImage);
            holder.textViewName = (TextView) convertView.findViewById(R.id.neoTxtName);
            holder.textViewDate2 = (TextView) convertView.findViewById(R.id.neoTxtDate);
            holder.textViewSize2 = (TextView) convertView.findViewById(R.id.neoTxtSize);
            //holder.textViewHazardous2 = (TextView) convertView.findViewById(R.id.neoTxtHazard);
            holder.textViewSize2 = (TextView) convertView.findViewById(R.id.neoTxtSize);
            holder.textViewLunar2 = (TextView) convertView.findViewById(R.id.neoTxtLunar);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        NearEarthObject neoRowItem = (NearEarthObject) getItem(position);

        //holder.imageView.setImageResource(R.mipmap.ic_launcher);
        holder.textViewName.setText(neoRowItem.getName());
        holder.textViewDate2.setText(neoRowItem.getDate());
        holder.textViewSize2.setText(neoRowItem.getEstimated_diameter_meters_min());
        if(neoRowItem.getPotentially_hazardous().equals("true")) {
            //convertView.findViewById(R.id.neoListRowId).setBackgroundColor(Color.RED);
            convertView.findViewById(R.id.neoListRowId).setBackgroundColor(Color.argb(80, 200, 0, 0));
        }
        //holder.textViewHazardous2.setText(neoRowItem.getHazard());
        holder.textViewLunar2.setText(neoRowItem.getMiss_distance_lunar());

        return convertView;
    }
}
