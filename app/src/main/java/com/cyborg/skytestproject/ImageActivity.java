package com.cyborg.skytestproject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        if(isNetworkAvailable()) {
            new ImageAsyncTask().execute();
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ImageActivity.this);
            alertDialogBuilder.setTitle("Internet connection");
            alertDialogBuilder
                    .setMessage("This app needs internet connection.")
                    .setCancelable(false);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if(item.getItemId() == R.id.action_NEO) {
            //Near Earth Objects
            Intent neoIntent = new Intent(this, NEOActivity.class);
            startActivity(neoIntent);
        }
        else if(item.getItemId() == R.id.action_image) {
            //Image of the day

        } else if(item.getItemId() == R.id.action_iss) {
            //Track ISS

            Intent issIntent = new Intent(this, TrackISSActivity.class);
            startActivity(issIntent);

        } else {
            // Settings
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private class ImageAsyncTask extends AsyncTask<String, String, String> {

        private String jsonStringUrl;
        private ImageOfDay imageOfDay;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            jsonStringUrl = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";

            progressDialog = new ProgressDialog(ImageActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY

            JsonParser jsonParser = new JsonParser();
            JSONObject jsonObject = jsonParser.getJSONFromUrl(jsonStringUrl);

            try {
                String date = jsonObject.getString("date");
                String explanation = jsonObject.getString("explanation");
                String media_type = jsonObject.getString("media_type");
                String title = jsonObject.getString("title");
                String url = jsonObject.getString("url");

                Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(url).getContent());

                imageOfDay = new ImageOfDay(date, title, explanation, url, bitmap, media_type);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            ImageView imageView = (ImageView) findViewById(R.id.imageOfTheDay);
            VideoView videoView = (VideoView) findViewById(R.id.videoOfTheDay);
            if(imageOfDay.getMedia_type().equals("image")) {
                if (imageOfDay.getBitmap() != null) {
                    videoView.setVisibility(View.INVISIBLE);
                    imageView.setImageBitmap(imageOfDay.getBitmap());
                }
            }
            else if(imageOfDay.getMedia_type().equals("video")) {
                imageView.setVisibility(View.INVISIBLE);
                //Show the video
            }

            System.out.println(imageOfDay.getName());
            TextView name = (TextView) findViewById(R.id.imageTitle);
            name.setText(imageOfDay.getName());

            System.out.println(imageOfDay.getExplanation());
            TextView explanation = (TextView) findViewById(R.id.imageExplanation);
            explanation.setText(imageOfDay.getExplanation());

            Button btnSave = (Button) findViewById(R.id.btnSaveImage);
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageView imageView = (ImageView) findViewById(R.id.imageOfTheDay);
                    BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
                    Bitmap bitmap = bitmapDrawable.getBitmap();

                    String sdCardDir = Environment.getExternalStorageDirectory().toString();

                    Calendar calendar = Calendar.getInstance();
                    Date today = calendar.getTime();

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                    String date = dateFormat.format(today);
                    String imageName = date + ".png";
                    File image = new File(sdCardDir, imageName);

                    boolean success = false;
                    FileOutputStream outputStream;
                    try {
                        outputStream = new FileOutputStream(image);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                        outputStream.flush();
                        outputStream.close();
                        success = true;
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (success) {
                        Toast.makeText(getApplicationContext(), "Image saved", Toast.LENGTH_SHORT).show();

                        try {
                            // Update the image gallery
                            MediaStore.Images.Media.insertImage(
                                    getContentResolver(),
                                    image.getAbsolutePath(),
                                    image.getName(),
                                    image.getName()
                            );

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Error during image saving", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            progressDialog.dismiss();

        }
    }
}
